﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataSetAntwerpen
{

    public class Rootobject
    {
        public Sport[] sport { get; set; }
        
    }

    public class Sport : IComparable<Sport>
    {
        public string title { get; set; }
        public string description { get; set; }
        public string address { get; set; }
        public string gisx { get; set; }
        public string gisy { get; set; }
        public string image { get; set; }
        public string link { get; set; }

        public int CompareTo(Sport other)
        {
           
            if (other == null) return 1;
            else
            {
                return this.title.CompareTo(other.title);
            }

        }

        public override string ToString()
        {
            return title;
        }
    }
}
