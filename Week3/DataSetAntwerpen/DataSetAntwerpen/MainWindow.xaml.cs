﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Newtonsoft.Json;

namespace DataSetAntwerpen
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void GetJSon_Click(object sender, RoutedEventArgs e)
        {
            string url = string.Format("http://api.antwerpen.be/v1/infrastructuur/sport.json");

            WebClient wc = new WebClient();

            string jsondata = wc.DownloadString(url);
            Rootobject data = JsonConvert.DeserializeObject<Rootobject>(jsondata);

            var result = (from sportinfr in data.sport where sportinfr.address != null select sportinfr).OrderBy(p=> p.title).ThenBy(p=>p.address);

            foreach (var sport in result)
            {
                sportItems.Items.Add(sport);                
            }
            
            GetJSon.IsEnabled = false;
        }

        private void sportItems_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ShowMoreInfo.IsEnabled = true;
            Sport item = new Sport();
            item = (Sport)(sportItems.SelectedItem);
            ExtraInfo.Text = item.description;
        }

        private void ShowMoreInfo_Click(object sender, RoutedEventArgs e)
        {
            Infoscherm info = new Infoscherm();
            Sport item = new Sport();
            item = (Sport)(sportItems.SelectedItem);
            info.UserTekst = item.title + "\n" + item.address + "\n" + item.description + "\n" + item.gisx + " " + item.gisy + "\n" + item.image + "\n" + item.link;
            info.ShowDialog();
        }
    }
}
