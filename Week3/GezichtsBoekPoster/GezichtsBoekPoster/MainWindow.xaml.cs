﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Facebook;
using Newtonsoft.Json;

namespace GezichtsBoekPoster
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
 
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            string wallAccessToken =
                Properties.Settings.Default.accessToken;
            string userid = Properties.Settings.Default.CurrentUser;
            string message = fbMessage.Text;
            var fb = new FacebookClient(wallAccessToken);
            string url = string.Format("{0}/{1}", userid, "feed");
            var argList = new Dictionary<string, object>();
            argList["message"] = message;
            try
            {
                fb.Post(url, argList);
            }
            catch (Exception f)
            {
                MessageBox.Show(f.Message);
            }
            
        }

        private void ComboBox_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            fbMessage.Text = ((ComboBoxItem)(defaultPosts.SelectedItem)).Content.ToString();
        }

        private void fbConnect_Click(object sender, RoutedEventArgs e)
        {
            ChangeUser CU = new ChangeUser();
            CU.ShowDialog();
        }

        private void prefabAdd_Click(object sender, RoutedEventArgs e)
        {
            string inhoud = fbMessage.Text;
            ComboBoxItem dirk = new ComboBoxItem();
            dirk.Content = inhoud;
            defaultPosts.Items.Add(dirk);
            Properties.Settings.Default.PrefabStrings.Add(inhoud);
            Properties.Settings.Default.Save();
        }

        private void GBPoster_Loaded(object sender, RoutedEventArgs e)
        {
            fbMessage.Text = Properties.Settings.Default.BeginText;
            for (int i = 0; i < Properties.Settings.Default.PrefabStrings.Count; i++)
            {
                ComboBoxItem ci = new ComboBoxItem();
                ci.Content = Properties.Settings.Default.PrefabStrings[i];
                defaultPosts.Items.Add(ci);
            }
        }

        private void CheckJson_Click(object sender, RoutedEventArgs e)
        {

            #region Dynamic JSON 
            /*
            string wallAccessToken =
                Properties.Settings.Default.accessToken;
            var fb = new FacebookClient(wallAccessToken);

            string url = string.Format("me?fields=id,name,relationship_status,significant_other");

            string mejson = fb.Get(url).ToString();
            dynamic result = JsonConvert.DeserializeObject(mejson);

            string signOther = " ";
            if (result.significant_other != null)
            {
                signOther = result.significant_other.ToString();
            }
            MessageBox.Show(result.id.ToString() + "\n" + result.name.ToString() + "\n" + result.relationship_status.ToString() + "\n" + signOther);
             */
            #endregion
        #region Non-Dynamic Way        
            string wallAccessToken =
                    Properties.Settings.Default.accessToken;
            var fb = new FacebookClient(wallAccessToken);

            string url = string.Format("me?fields=id,name,relationship_status,significant_other");

            string mejson = fb.Get(url).ToString();
            try
            {
                UserInfo result = JsonConvert.DeserializeObject<UserInfo>(mejson);
                string signOther = " ";
                if (result.significant_other != null)
                {
                    signOther = result.significant_other.ToString();
                }
                MessageBox.Show(result.id.ToString() + "\n" + result.name.ToString() + "\n" + result.relationship_status.ToString() + "\n" + signOther);
            }
            catch (Exception f)
            {

                MessageBox.Show(f.Message);
            }


            
        #endregion
        }

        public class UserInfo
        {
            public string id { get; set; }
            public string name { get; set; }
            public string relationship_status { get; set; }
            public string significant_other { get; set; }
        }

    }
}
