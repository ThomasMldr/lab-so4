﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GezichtsBoekPoster
{
    /// <summary>
    /// Interaction logic for ChangeUser.xaml
    /// </summary>
    public partial class ChangeUser : Window
    {
        public ChangeUser()
        {
            InitializeComponent();
        }

        private void Login_Click(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.CurrentUser = NewUser.Text;
            Properties.Settings.Default.accessToken = accessT.Text;
            Properties.Settings.Default.Save();
            Close();
        }

        private void ComboBox_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            NewUser.Text = ((ComboBoxItem)(ExistUsers.SelectedItem)).Content.ToString();
        }

        private void SaveUser_Click(object sender, RoutedEventArgs e)
        {
            string inhoud = NewUser.Text;
            ComboBoxItem dirk = new ComboBoxItem();
            dirk.Content = inhoud;
            ExistUsers.Items.Add(dirk);
            Properties.Settings.Default.Users.Add(inhoud);
            Properties.Settings.Default.Save();
        }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < Properties.Settings.Default.Users.Count; i++)
            {
                ComboBoxItem ci = new ComboBoxItem();
                ci.Content = Properties.Settings.Default.Users[i];
                ExistUsers.Items.Add(ci);
            }
        }
    }
}
