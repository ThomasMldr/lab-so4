﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace SilverPokedex
{
    public partial class MainPage : UserControl
    {
        bool filtered = false;
        int minlvl = 0;
        public ObservableCollection<Pokémon> Pokémons { get; set; }
        public void MainWindow()
        {
            InitializeComponent();
            Pokémons = new ObservableCollection<Pokémon>();
            
            #region hardcode aanmaken pokémon
            ////Pokémon worden bij laden venster reeds ingeladen, hieronder hardcoded van voorheen
            //{
            //    new Pokémon()
            //        {
            //            Name = "Bulbasaur",
            //            Number = 1,
            //            Type = "Grass",
            //            ImageUrl = "http://i903.photobucket.com/albums/ac234/GVSG/Pokemon%20PNGs/Bulbasaur.png",
            //            HP_Base = 45,
            //            Attack_Base = 49,
            //            Defense_Base = 49,
            //            SpecialAttack_Base = 65,
            //            SpecialDefense_Base = 65,
            //            Speed_Base = 45, 
            //            Level = 103
            //        },
            //    new Pokémon()
            //        {
            //            Name ="Charmander",
            //            Number =4,
            //            Type ="Fire",
            //            ImageUrl ="http://0-media-cdn.foolz.us/ffuuka/board/vp/image/1365/05/1365055899070.png",
            //            HP_Base =39,
            //            Attack_Base =52,
            //            Defense_Base =43,
            //            SpecialAttack_Base =60,
            //            SpecialDefense_Base =50,
            //            Speed_Base =65, 
            //            Level = 100
            //        },
            //    new Pokémon()
            //        {
            //            Name ="Squirtle",
            //            Number =7,
            //            Type ="Water",
            //            ImageUrl ="http://img3.wikia.nocookie.net/__cb20111211170902/villains/images/e/e3/Squirtle.png",
            //            HP_Base =44,
            //            Attack_Base =48,
            //            Defense_Base =65,
            //            SpecialAttack_Base =50,
            //            SpecialDefense_Base =64,
            //            Speed_Base =43, 
            //            Level = 90
            //        }
            //};
            #endregion

            
            #region JsonSerializer
            var serializer = new JsonSerializer();
            using (var re = File.OpenText("json.txt"))
            using (var reader = new JsonTextReader(re))
            {
                var entries = serializer.Deserialize<Pokémon[]>(reader);
                foreach (var polkadanser in entries)
                {
                    Pokémons.Add(polkadanser);
                }
            }
            #endregion

            PokéFlip.DataContext = Pokémons;
            PokéStats.DataContext = Pokémons[0];

        }

        private void FlipView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var flipview = ((TabControl)sender);
            try
            {
                
                int sI = flipview.SelectedIndex;
                //flipview. = Pokémons[sI].Name;
                PokéStats.DataContext = Pokémons[sI];
            }
            catch (Exception)
            {
                //flipview.BannerText = "Pokémon";
                PokéStats.DataContext = Pokémons;

            }
        }

        private void MetroWindow_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            if (btn.DataContext is Pokémon)
            {
                Pokémon pokemon = (Pokémon)btn.DataContext;
                pokemon.VerhoogLevel();
            }
        }

        private void AddPoké_Click(object sender, RoutedEventArgs e)
        {
            AddPokémon addPokém = new AddPokémon();
            addPokém.Show();
            try
            {
                Pokémon pok = new Pokémon()
                    {
                        Name = addPokém.AddName.Text,
                        Number = Convert.ToInt32(addPokém.AddNumber.Text),
                        Type = addPokém.AddType.Text,
                        ImageUrl = addPokém.AddImg.Text,
                        HP_Base = Convert.ToInt32(addPokém.AddHP.Text),
                        Attack_Base = Convert.ToInt32(addPokém.AddAtt.Text),
                        Defense_Base = Convert.ToInt32(addPokém.AddDef.Text),
                        SpecialAttack_Base = Convert.ToInt32(addPokém.AddSpecAtt.Text),
                        SpecialDefense_Base = Convert.ToInt32(addPokém.AddSpecDef.Text),
                        Speed_Base = Convert.ToInt32(addPokém.AddSpeed.Text),
                        Level = Convert.ToInt32(addPokém.AddLevel.Text)
                    };
                Pokémons.Add(pok);
            }
            catch (FormatException)
            {
                MessageBox.Show("Pokemon not added, try again and pay extra attention to what you type!", "Not Added", MessageBoxButton.OK);

            }

            addPokém.Close();
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            //Serialize to JSON
            JsonSerializer serializer = new JsonSerializer();
            serializer.NullValueHandling = NullValueHandling.Ignore;

            using (StreamWriter sw = new StreamWriter(@"json.txt"))
            using (JsonWriter writer = new JsonTextWriter(sw))
            {
                serializer.Serialize(writer, Pokémons);
            }
        }

        private void Poster_Click(object sender, RoutedEventArgs e) 
        {
            int sI = PokéFlip.SelectedIndex;
            ShowPokémon ShP = new ShowPokémon();

            var powk = PokéFlip.SelectedItem;
            if (powk != null)
            {
                Uri pUri = new Uri(Pokémons[sI].ImageUrl);
                BitmapImage imageBitmap = new BitmapImage(pUri);
                Image myImage = new Image();
                ShP.PokéPoster.Source = imageBitmap;
                ShP.PokéName.Text = Pokémons[sI].Name;
            }

            ShP.Show();

            ShP.Close();
        }

        void ShowFirst(object sender, RoutedEventArgs e)
        {
            this.fly.IsOpen = !this.fly.IsOpen;
        }

        private void Filter_Click(object sender, RoutedEventArgs e)
        {
            minlvl = Convert.ToInt32(Level.Text);
            if (minlvl == 0)
            {
                filtered = false;
            }
            else filtered = true;
            if (Level.Text != null)
            {
                if (Convert.ToInt32(Level.Text) == 0)
                {
                    var serializer = new JsonSerializer();
                    Pokémons.Clear();
                    using (var re = File.OpenText("json.txt"))
                    using (var reader = new JsonTextReader(re))
                    {
                        var entries = serializer.Deserialize<Pokémon[]>(reader);
                        foreach (var polkadanser in entries)
                        {
                            Pokémons.Add(polkadanser);
                        }
                    }
                }
                else
                {
                    try
                    {
                       var serializer = new JsonSerializer(); 
                        Pokémons.Clear();
                        using (var re = File.OpenText("json" + minlvl + ".txt"))
                        using (var reader = new JsonTextReader(re))
                        {
                            var entries = serializer.Deserialize<Pokémon[]>(reader);
                            foreach (var polkadanser in entries)
                            {
                                Pokémons.Add(polkadanser);
                            }
                        }
                    }
                    catch (Exception bla)
                    {
                        MessageBox.Show(bla.ToString() + bla.Message);
                    }
                }
                //reset the datacontext to refresh flipview
                PokéFlip.DataContext = null;
                PokéFlip.DataContext = Pokémons;
                PokéStats.DataContext = null;
                PokéStats.DataContext = Pokémons[0];
            }
        }
        //static Pokémon DeSerializer(XElement element)
        //{
        //    var serializer = new XmlSerializer(typeof(Pokémon));
        //    return (Pokémon)serializer.Deserialize(element.CreateReader());
        //}//serializen zorgt voor wat problemen bij silverlight precies..
    }
}