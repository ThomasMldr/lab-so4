﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using MenuControl;

namespace sampleMenu
{
    public partial class Page : UserControl
    {
        public Page()
        {
            InitializeComponent();
            this.Loaded += new RoutedEventHandler(Page_Loaded);
        }

        void Page_Loaded(object sender, RoutedEventArgs e)
        {
            // an example of how to create the control programatically           
            //MenuBarItem it1 = new MenuBarItem();
            //it1.MenuText = "File";
            //MenuItem f1 = new MenuItem();
            //f1.MenuText = "new item";
            //f1.Click += MenuItem_Click;
            //it1.items.Add(f1);
            //MenuItem f2 = new MenuItem();
            //f2.MenuText = "new item 2";
            //f2.Click += MenuItem_Click;
            //it1.items.Add(f2);
            //MenuItem f3 = new MenuItem();
            //f3.MenuText = "very";
            //f3.Click += MenuItem_Click;
            //it1.items.Add(f3);
            //MenuItem f4 = new MenuItem();
            //f4.MenuText = "largeeeeeeeeee";
            //f4.Click += MenuItem_Click;
            //it1.items.Add(f4);

            //MenuBarItem it2 = new MenuBarItem();
            //it2.MenuText = "Edit";
            //MenuItem a1 = new MenuItem();
            //a1.MenuText = "new item";
            //a1.Click += MenuItem_Click;
            //it2.items.Add(a1);
            //MenuItem a2 = new MenuItem();
            //a2.MenuText = "new nested item ";
            //MenuItem nested1 = new MenuItem();
            //nested1.MenuText = "I am nested.";
            //nested1.Click += MenuItem_Click;
            //a2.items.Add(nested1);
            //it2.items.Add(a2);

            //MenuBarItem it3 = new MenuBarItem();
            //it3.MenuText = "Help";
            //MenuItem b1 = new MenuItem();
            //b1.MenuText = "this";
            //b1.Click += MenuItem_Click;
            //it3.items.Add(b1);
            //MenuItem b2 = new MenuItem();
            //b2.MenuText = "that";
            //b2.Click += MenuItem_Click;
            //it3.items.Add(b2);
            //MenuItem b3 = new MenuItem();
            //b3.MenuText = "that and that this";
            //b3.Click += MenuItem_Click;
            //it3.items.Add(b3);
            

            //MenuBar bar = new MenuBar();
            //bar.items.Add(it1);
            //bar.items.Add(it2);
            //bar.items.Add(it3);

            //LRoot.Children.Add(bar);
        }

        

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Browser.HtmlPage.Window.Alert("you pressed: " + (sender as MenuItem).MenuText);

        }
    }
}
