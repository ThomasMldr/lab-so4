﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LINQitWPF
{/// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 
    // extra score indien je de oplossingen toont in een wpf appplicatie waarbij iedere query achter
    // een knop staat en die vervolgens het resultaat toont in een databound listbox)

    public partial class MainWindow : Window
    {
        Ruimteschip[] schepen = new Ruimteschip[]
        {
            new Ruimteschip() { Naam = "SS Enterprise", Bewapend = true, Capaciteit = 5000, Snelheid = 4 }, 
            new Ruimteschip() { Naam = "Death Star", Bewapend = true, Capaciteit = 50000, Snelheid = 0 }, 
            new Ruimteschip() { Naam = "Voyager", Bewapend = false, Capaciteit = 3, Snelheid = 1 }, 
            new Ruimteschip() { Naam = "Norg Cube", Bewapend = true, Capaciteit = 150000, Snelheid = 2 }, 
            new Ruimteschip() { Naam = "Death Star", Bewapend = true, Capaciteit = 50000, Snelheid = 0 }, 
            new Ruimteschip() { Naam = "TieFighter", Bewapend = true, Capaciteit = 1, Snelheid = 2 }, 
            new Ruimteschip() { Naam = "Imperial Shuttle", Bewapend = false, Capaciteit = 3, Snelheid = 1 }
        };

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click1(object sender, RoutedEventArgs e)
        {
            Mission.Text = ("1. Alle schepen die bewapend zijn") + ("\nvar armed = from gun in schepen where gun.Bewapend == true select gun;");
            var armed = from gun in schepen where gun.Bewapend == true select gun;
            Output.Text = "";
            foreach (Ruimteschip space in armed)
            {
                Output.Text += (space.Naam + " - " + space.Bewapend +"\n");
            }
        }

        private void Button_Click2(object sender, RoutedEventArgs e)
        {
            Mission.Text = ("2. Alle schepen die trager dan 3 snelheid hebben en niet bewapend zijn");
            Mission.Text += ("\nvar slowunarmed = from gun in schepen where gun.Bewapend == false && gun.Snelheid < 3 select gun;");
            var slowunarmed = from gun in schepen where gun.Bewapend == false && gun.Snelheid < 3 select gun;
            Output.Text = "";
            foreach (Ruimteschip ruimteschip in slowunarmed)
            {
                Output.Text += (ruimteschip.Naam + " - " + ruimteschip.Bewapend + " - " + ruimteschip.Snelheid + "\n");
            }
        }

        private void Button_Click3(object sender, RoutedEventArgs e)
        {
            Mission.Text = ("3. Groepeer de schepen op het feit of ze bewapend zijn of niet");
            Mission.Text += ("\nvar armedgroup = schepen.GroupBy(p => p.Bewapend).Select(p => p);");
            var armedgroup = schepen.GroupBy(p => p.Bewapend).Select(p => p);
            Output.Text = "";
            foreach (var ruimteschip in armedgroup)
            {
                foreach (Ruimteschip ruimteschip1 in ruimteschip)
                {
                    Output.Text += (ruimteschip1.Naam + " - " + ruimteschip1.Bewapend + "\n");
                }
            }
        }

        private void Button_Click4(object sender, RoutedEventArgs e)
        {
            Mission.Text = ("4. Tel het aantal schepen wiens snelheid gelijk is aan 1");
            Mission.Text += ("\n(from schip in schepen where schip.Snelheid == 1 select schip).Count()");
            int countOne = (from schip in schepen where schip.Snelheid == 1 select schip).Count();
            Output.Text = ("Aantal schepen:" + countOne);
        }

        private void Button_Click5(object sender, RoutedEventArgs e)
        {
            Mission.Text = ("5. Toon het eerst schip wiens capaciteit 3 is");
            Mission.Text += ("\n(from gun in schepen where gun.Capaciteit == 3 select gun).FirstOrDefault()");
            Ruimteschip firstcap3 = (from gun in schepen where gun.Capaciteit == 3 select gun).FirstOrDefault();
            Output.Text = (firstcap3.Naam + " - " + firstcap3.Capaciteit);
        }

        private void Button_Click6(object sender, RoutedEventArgs e)
        {
            Mission.Text = ("6. Order de schepen op capaciteit");
            Mission.Text += ("\nvar capOrder = schepen.OrderBy(p => p.Capaciteit).Select(p => p);");
            var capOrder = schepen.OrderBy(p => p.Capaciteit).Select(p => p);
            Output.Text = "";
            foreach (var ruimteschip in capOrder)
            {
                Output.Text += (ruimteschip.Naam + " - " + ruimteschip.Capaciteit +"\n");
            }
            var capOrderDesc = schepen.OrderByDescending(p => p.Capaciteit).Select(p => p);
            Output.Text += ("\nAnd now Descending: ");
            Output.Text += ("\nvar capOrderDesc = schepen.OrderByDescending(p => p.Capaciteit).Select(p => p);");
            foreach (var ruimteschip in capOrderDesc)
            {
                Output.Text += (ruimteschip.Naam + " - " + ruimteschip.Capaciteit +"\n");
            }
        }

        private void Button_Click7(object sender, RoutedEventArgs e)
        {
            Mission.Text = ("7. Order de schepen op snelheid en toon het laatste schip");
            Mission.Text += ("\nRuimteschip slowest = (schepen.OrderByDescending(p => p.Snelheid).Select(p => p)).LastOrDefault();");
            Ruimteschip slowest = (schepen.OrderByDescending(p => p.Snelheid).Select(p => p)).LastOrDefault();
            Output.Text = (slowest.Naam + " - " + slowest.Snelheid);
            Output.Text += ("\nOr did you mean the fastest ship?");
            Output.Text += ("\nRuimteschip fastest = (schepen.OrderBy(p => p.Snelheid).Select(p => p)).LastOrDefault();");
            Ruimteschip fastest = (schepen.OrderBy(p => p.Snelheid).Select(p => p)).LastOrDefault();
            Output.Text += ("\n" + fastest.Naam + " - " + fastest.Snelheid);
        }

        private void Button_Click8(object sender, RoutedEventArgs e)
        {
            Mission.Text = ("8. Neem de eerste 2 schepen die een capaciteit hebben groter dan 5");
            Mission.Text += ("\nvar largerCap5 = (from schip in schepen where schip.Capaciteit > 5 orderby schip.Capaciteit descending select schip).Take(2);");
            var largerCap5 =
                (from schip in schepen where schip.Capaciteit > 5 orderby schip.Capaciteit descending select schip).Take(2);
            Output.Text = "";
            foreach (var ruimteschip in largerCap5)
            {
                Output.Text += (ruimteschip.Naam + " - " + ruimteschip.Capaciteit+ "\n");
            }
        }
    }
}