﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQit
{
    class Program
    {
        static void Main(string[] args)
        {
            /* 1° Maak een aparte console-applicatie waarin je de verschillende linq queries aantoont
                 * die je moet maken in de opgave in bijlage. Je toont telkens via WriteLine welke query je 
                 * toont (bv Console.WriteLine("from p in ...") en dan het resultaat van de query (bv opsomming 
                 * van de ruimteschepen mbv foreach)
                 * extra score indien je de oplossingen toont in een wpf appplicatie waarbij iedere query achter
                 * een knop staat en die vervolgens het resultaat toont in een databound listbox)
             * 2° Verzin de meeste stoere, coole, knappe linq query en pas deze toe in je linq-opgave van vorige week.
             * 
             *  1. Alle schepen die bewapend zijn
                2. Alle schepen die trager dan 3 snelheid hebben en niet bewapend zijn
                3. Groepeer de schepen op het feit of ze bewapend zijn of niet
                4. Tel het aantal schepen wiens snelheid gelijk is aan 1
                5. Toon het eerst schip wiens capaciteit 3 is
                6. Order de schepen op capaciteit
                7. Order de schepen op snelheid en toon het laatste schip
                8. Neem de eerste 2 schepen die een capaciteit hebben groter dan 5
             */

            Ruimteschip[] schepen = new Ruimteschip[]
                {
                    new Ruimteschip() { Naam = "SS Enterprise", Bewapend = true, Capaciteit = 5000, Snelheid = 4 }, 
                    new Ruimteschip() { Naam = "Death Star", Bewapend = true, Capaciteit = 50000, Snelheid = 0 }, 
                    new Ruimteschip() { Naam = "Voyager", Bewapend = false, Capaciteit = 3, Snelheid = 1 }, 
                    new Ruimteschip() { Naam = "Norg Cube", Bewapend = true, Capaciteit = 150000, Snelheid = 2 }, 
                    new Ruimteschip() { Naam = "Death Star", Bewapend = true, Capaciteit = 50000, Snelheid = 0 }, 
                    new Ruimteschip() { Naam = "TieFighter", Bewapend = true, Capaciteit = 1, Snelheid = 2 }, 
                    new Ruimteschip() { Naam = "Imperial Shuttle", Bewapend = false, Capaciteit = 3, Snelheid = 1 }
                };
            Console.WriteLine("1. Alle schepen die bewapend zijn");
            Console.WriteLine("var armed = from gun in schepen where gun.Bewapend == true select gun;");
            var armed = from gun in schepen where gun.Bewapend == true select gun;
            foreach (Ruimteschip space in armed)
            {
                Console.WriteLine(space.Naam + " - " + space.Bewapend);
            }

            Console.WriteLine("\n2. Alle schepen die trager dan 3 snelheid hebben en niet bewapend zijn");
            Console.WriteLine("var slowunarmed = from gun in schepen where gun.Bewapend == false && gun.Snelheid < 3 select gun;");
            var slowunarmed = from gun in schepen where gun.Bewapend == false && gun.Snelheid < 3 select gun;
            foreach (Ruimteschip ruimteschip in slowunarmed)
            {
                Console.WriteLine(ruimteschip.Naam + " - " + ruimteschip.Bewapend + " - " + ruimteschip.Snelheid);
            }

            Console.WriteLine("\n3. Groepeer de schepen op het feit of ze bewapend zijn of niet");
            Console.WriteLine("var armedgroup = schepen.GroupBy(p => p.Bewapend).Select(p => p);");
            var armedgroup = schepen.GroupBy(p => p.Bewapend).Select(p => p);
            foreach (var ruimteschip in armedgroup)
            {
                foreach (Ruimteschip ruimteschip1 in ruimteschip)
                {
                    Console.WriteLine(ruimteschip1.Naam + " - " + ruimteschip1.Bewapend);
                }
            }

            Console.WriteLine("\n4. Tel het aantal schepen wiens snelheid gelijk is aan 1");
            Console.WriteLine("(from schip in schepen where schip.Snelheid == 1 select schip).Count()");
            int countOne = (from schip in schepen where schip.Snelheid == 1 select schip).Count();
            Console.WriteLine("Aantal schepen:" + countOne);

            Console.WriteLine("\n5. Toon het eerst schip wiens capaciteit 3 is");
            Console.WriteLine("(from gun in schepen where gun.Capaciteit == 3 select gun).FirstOrDefault()");
            Ruimteschip firstcap3 = (from gun in schepen where gun.Capaciteit == 3 select gun).FirstOrDefault();
            Console.WriteLine(firstcap3.Naam + " - " + firstcap3.Capaciteit);

            Console.WriteLine("\n6. Order de schepen op capaciteit");
            Console.WriteLine("var capOrder = schepen.OrderBy(p => p.Capaciteit).Select(p => p);");
            var capOrder = schepen.OrderBy(p => p.Capaciteit).Select(p => p);
            foreach (var ruimteschip in capOrder)
            {
                Console.WriteLine(ruimteschip.Naam + " - " + ruimteschip.Capaciteit);
            }
            var capOrderDesc = schepen.OrderByDescending(p => p.Capaciteit).Select(p => p);
            Console.WriteLine("\nAnd now Descending: ");
            Console.WriteLine("var capOrderDesc = schepen.OrderByDescending(p => p.Capaciteit).Select(p => p);");
            foreach (var ruimteschip in capOrderDesc)
            {
                Console.WriteLine(ruimteschip.Naam + " - " + ruimteschip.Capaciteit);
            }

            Console.WriteLine("\n7. Order de schepen op snelheid en toon het laatste schip");
            Console.WriteLine("Ruimteschip slowest = (schepen.OrderByDescending(p => p.Snelheid).Select(p => p)).LastOrDefault();");
            Ruimteschip slowest = (schepen.OrderByDescending(p => p.Snelheid).Select(p => p)).LastOrDefault();
            Console.WriteLine(slowest.Naam + " - " + slowest.Snelheid);
            Console.WriteLine("\nOr did you mean the fastest ship?");
            Console.WriteLine("Ruimteschip fastest = (schepen.OrderBy(p => p.Snelheid).Select(p => p)).LastOrDefault();");
            Ruimteschip fastest = (schepen.OrderBy(p => p.Snelheid).Select(p => p)).LastOrDefault();
            Console.WriteLine(fastest.Naam + " - " + fastest.Snelheid);


            Console.WriteLine("\n8. Neem de eerste 2 schepen die een capaciteit hebben groter dan 5");
            Console.WriteLine("var largerCap5 = (from schip in schepen where schip.Capaciteit > 5 orderby schip.Capaciteit descending select schip).Take(2);");
            var largerCap5 =
                (from schip in schepen where schip.Capaciteit > 5 orderby schip.Capaciteit descending select schip).Take(2);
            foreach (var ruimteschip in largerCap5)
            {
                Console.WriteLine(ruimteschip.Naam + " - " + ruimteschip.Capaciteit);
            }

            

            Console.ReadLine();
        }
        class Ruimteschip
        {
            public string Naam { get; set; } 
            public int Snelheid { get; set; } 
            public int Capaciteit { get; set; } 
            public bool Bewapend { get; set; }
        }
    }
}
