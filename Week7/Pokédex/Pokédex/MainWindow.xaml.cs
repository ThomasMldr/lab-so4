﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Pokédex
{
    public partial class MainWindow : MetroWindow
    {
        bool filtered = false;
        int minlvl = 0;
        public ObservableCollection<Pokémon> Pokémons { get; set; }
        public MainWindow()
        {
            InitializeComponent();
            Pokémons = new ObservableCollection<Pokémon>();
            #region hardcode aanmaken pokémon
            ////Pokémon worden bij laden venster reeds ingeladen, hieronder hardcoded van voorheen
            //{
            //    new Pokémon()
            //        {
            //            Name = "Bulbasaur",
            //            Number = 1,
            //            Type = "Grass",
            //            ImageUrl = "http://i903.photobucket.com/albums/ac234/GVSG/Pokemon%20PNGs/Bulbasaur.png",
            //            HP_Base = 45,
            //            Attack_Base = 49,
            //            Defense_Base = 49,
            //            SpecialAttack_Base = 65,
            //            SpecialDefense_Base = 65,
            //            Speed_Base = 45, 
            //            Level = 103
            //        },
            //    new Pokémon()
            //        {
            //            Name ="Charmander",
            //            Number =4,
            //            Type ="Fire",
            //            ImageUrl ="http://0-media-cdn.foolz.us/ffuuka/board/vp/image/1365/05/1365055899070.png",
            //            HP_Base =39,
            //            Attack_Base =52,
            //            Defense_Base =43,
            //            SpecialAttack_Base =60,
            //            SpecialDefense_Base =50,
            //            Speed_Base =65, 
            //            Level = 100
            //        },
            //    new Pokémon()
            //        {
            //            Name ="Squirtle",
            //            Number =7,
            //            Type ="Water",
            //            ImageUrl ="http://img3.wikia.nocookie.net/__cb20111211170902/villains/images/e/e3/Squirtle.png",
            //            HP_Base =44,
            //            Attack_Base =48,
            //            Defense_Base =65,
            //            SpecialAttack_Base =50,
            //            SpecialDefense_Base =64,
            //            Speed_Base =43, 
            //            Level = 90
            //        }
            //};
            #endregion

            var entree = LoadFromXml<ObservableCollection<Pokémon>>("pokexml.xml");
            foreach (var p in entree)
            {
                Pokémons.Add(p);
            }

            //var enter = LoadFromXmlAsync<ObservableCollection<Pokémon>>("pokexml.xml");
            //foreach (var p in enter.Result)
            //{
            //    Pokémons.Add(p);
            //}

            #region JsonSerializer
            //var serializer = new JsonSerializer();
            //using (var re = File.OpenText("json.txt"))
            //using (var reader = new JsonTextReader(re))
            //{
            //    var entries = serializer.Deserialize<Pokémon[]>(reader);
            //    foreach (var polkadanser in entries)
            //    {
            //        Pokémons.Add(polkadanser);
            //    }
            //}
            #endregion

            PokéFlip.DataContext = Pokémons;
            PokéStats.DataContext = Pokémons[0];

        }

        private void FlipView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var flipview = ((FlipView)sender);
            try
            {
                
                int sI = flipview.SelectedIndex;
                flipview.BannerText = Pokémons[sI].Name;
                PokéStats.DataContext = Pokémons[sI];
            }
            catch (Exception)
            {
                flipview.BannerText = "Pokémon";
                PokéStats.DataContext = Pokémons;

            }
        }

        private void MetroWindow_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            if (btn.DataContext is Pokémon)
            {
                Pokémon pokemon = (Pokémon)btn.DataContext;
                pokemon.VerhoogLevel();
            }
        }

        private void SerializePokémon(ObservableCollection<Pokémon> p) //To XML
        {
            if (filtered == false)
            {
                var serializer = new XmlSerializer(p.GetType(), new XmlRootAttribute("Pokémons"));
                using (var writer = XmlWriter.Create("pokexml.xml"))
                {
                    serializer.Serialize(writer, p);
                }
            }
            else
            {
                var serializer = new XmlSerializer(p.GetType(), new XmlRootAttribute("Pokémons"));
                using (var writer = XmlWriter.Create("pokexml" + minlvl + ".xml"))
                {
                    serializer.Serialize(writer, p);
                }
            }
        }



        public static T LoadFromXml<T>(string filepath)
        {
            var serializer = new XmlSerializer(typeof(T), new XmlRootAttribute("Pokémons"));
            using (var reader = XmlReader.Create(filepath))
            {
                return (T)serializer.Deserialize(reader);
            }
        }

        #region poging tot async maken, gene vette precies
        public static async Task<T> LoadFromXmlAsync<T>(string filepath)
        {
           var serializer = new XmlSerializer(typeof(T), new XmlRootAttribute("Pokémons"));
           using (var reader = XmlReader.Create(filepath))
           {
               var read = await Task.Run(()=>serializer.Deserialize(reader));
               return (T)read;
           }
        }
        #endregion

        private void AddPoké_Click(object sender, RoutedEventArgs e)
        {
            AddPokémon addPokém = new AddPokémon();
            addPokém.ShowDialog();
            try
            {
                Pokémon pok = new Pokémon()
                    {
                        Name = addPokém.AddName.Text,
                        Number = Convert.ToInt32(addPokém.AddNumber.Text),
                        Type = addPokém.AddType.Text,
                        ImageUrl = addPokém.AddImg.Text,
                        HP_Base = Convert.ToInt32(addPokém.AddHP.Text),
                        Attack_Base = Convert.ToInt32(addPokém.AddAtt.Text),
                        Defense_Base = Convert.ToInt32(addPokém.AddDef.Text),
                        SpecialAttack_Base = Convert.ToInt32(addPokém.AddSpecAtt.Text),
                        SpecialDefense_Base = Convert.ToInt32(addPokém.AddSpecDef.Text),
                        Speed_Base = Convert.ToInt32(addPokém.AddSpeed.Text),
                        Level = Convert.ToInt32(addPokém.AddLevel.Text)
                    };
                Pokémons.Add(pok);
            }
            catch (FormatException)
            {
                this.ShowMessageAsync("Not Added", "Pokemon not added, try again and pay extra attention to what you type!");
            }

            addPokém.Close();
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            //Serialize to XML
            SerializePokémon(Pokémons);

            //Serialize to JSON
            JsonSerializer serializer = new JsonSerializer();
            serializer.NullValueHandling = NullValueHandling.Ignore;

            using (StreamWriter sw = new StreamWriter(@"json.txt"))
            using (JsonWriter writer = new JsonTextWriter(sw))
            {
                serializer.Serialize(writer, Pokémons);
            }
        }

        private void Poster_Click(object sender, RoutedEventArgs e) //Dit zou efficiënter kunnen, louter om de Linq te gebruiken...
        {
            int sI = PokéFlip.SelectedIndex;
            ShowPokémon ShP = new ShowPokémon();
            var powk = (from pok in Pokémons where pok.Name == PokéFlip.BannerText select pok.ImageUrl).FirstOrDefault();
            if (powk != null)
            {
                Uri pUri = new Uri(powk);
                BitmapImage imageBitmap = new BitmapImage(pUri);
                Image myImage = new Image();
                ShP.PokéPoster.Source = imageBitmap;
                ShP.PokéName.Text = (from pok in Pokémons where pok.Name == PokéFlip.BannerText select pok.Name).FirstOrDefault();
            }

            ShP.ShowDialog();

            ShP.Close();
        }

        void ShowFirst(object sender, RoutedEventArgs e)
        {
            this.fly.IsOpen = !this.fly.IsOpen;
        }

        private void Filter_Click(object sender, RoutedEventArgs e)
        {
            minlvl = Convert.ToInt32(Level.Text);
            if (minlvl == 0)
            {
                filtered = false;
            }
            else filtered = true;
            if (Level.Text != null)
            {
                if (Convert.ToInt32(Level.Text) == 0)
                {
                    var entree = LoadFromXml<ObservableCollection<Pokémon>>("pokexml.xml");
                    Pokémons.Clear();
                    foreach (var p in entree)
                    {
                        Pokémons.Add(p);
                    }
                }
                else
                {
                    try
                    {
                        var entree = LoadFromXml<ObservableCollection<Pokémon>>("pokexml" + minlvl + ".xml");
                        Pokémons.Clear();
                        foreach (var p in entree)
                        {
                            Pokémons.Add(p);
                        }
                    }
                    catch (Exception)
                    {
                        IEnumerable<XElement> minLvlPok =
                        from min in XDocument.Load("pokexml.xml").Descendants("Pokémons").Descendants("Pokémon")
                        where Convert.ToInt32(min.Element("Level").Value) >= Convert.ToInt32(Level.Text)
                        orderby (string)min.Element("Level")
                        select min;
                        Pokémons.Clear();
                        foreach (var xPok in minLvlPok)
                        {
                            Pokémons.Add(DeSerializer(xPok));
                        }
                    }
                }
                //reset the datacontext to refresh flipview
                PokéFlip.DataContext = null;
                PokéFlip.DataContext = Pokémons;
                PokéStats.DataContext = null;
                PokéStats.DataContext = Pokémons[0];
            }
        }
        static Pokémon DeSerializer(XElement element)
        {
            var serializer = new XmlSerializer(typeof(Pokémon));
            return (Pokémon)serializer.Deserialize(element.CreateReader());
        }
    }
}
