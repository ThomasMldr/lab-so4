﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DataSetAntwerpen
{
    /// <summary>
    /// Interaction logic for AddUserScherm.xaml
    /// </summary>
    public partial class AddUserScherm : Window
    {
        public AddUserScherm()
        {
            InitializeComponent();
        }

        private void AddNew_Click(object sender, RoutedEventArgs e)
        {
            if (AddDescription.Text != "" && AddGisx.Text != "" && AddGisy.Text != "" && AddAddress.Text != "" && AddLink.Text != "" &&
                AddTitle.Text != "")
            {
                
                Sport sp = new Sport() { address = AddAddress.Text, description = AddDescription.Text, gisx = AddGisx.Text, gisy = AddGisy.Text, image = "", link = AddLink.Text, title = AddTitle.Text };
                MessageBox.Show("New sports facility added!", "Added", MessageBoxButton.OK, MessageBoxImage.Information);
                this.Close();
                
            }
            else
            {
                MessageBox.Show("Not all of your fields are correctly filled in, please check this",
                                "One or more empty fields", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
