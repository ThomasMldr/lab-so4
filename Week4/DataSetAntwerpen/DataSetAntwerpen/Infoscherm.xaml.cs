﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DataSetAntwerpen
{
    /// <summary>
    /// Interaction logic for Infoscherm.xaml
    /// </summary>
    public partial class Infoscherm : Window
    {
        public Infoscherm()
        {
            InitializeComponent();
        }

        private string usertekst;

        public string UserTekst
        {
            get
            {
                return usertekst;
            }
            set
            {
                usertekst = value;
                infoTxb.Text = usertekst;
            }
        }
    }
}
