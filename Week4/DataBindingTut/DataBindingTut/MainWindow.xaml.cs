﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DataBindingTut
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public ObservableCollection<User> Users;

        public MainWindow()
        {
            InitializeComponent();
            Users = new ObservableCollection<User>()
                {
                    new User() {Name = "Tim", Age = 30},
                    new User() {Name = "Tom", Age = 31},
                    new User() {Name = "Thomas", Age = 22},
                    new User() {Name = "Thijs", Age = 33},
                    new User() {Name = "Tijl", Age = 34},
                    new User() {Name = "Theo", Age = 35}
                };
            lbUsers.DataContext = Users;
        }

        private void cmdDeleteUser_Clicked(object sender, RoutedEventArgs e)
        {
            Button cmd = (Button) sender;
            if (cmd.DataContext is User)
            {
                User deleteme = (User) cmd.DataContext;
                Users.Remove(deleteme);
            }
        }


    }
}
