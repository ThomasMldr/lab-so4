﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;

namespace Pokédex
{
    /// <summary>
    /// Interaction logic for AddPokémon.xaml
    /// </summary>
    public partial class AddPokémon : MetroWindow
    {
        public AddPokémon()
        {
            InitializeComponent();
        }

        private void AddNew_Click(object sender, RoutedEventArgs e)
        {
            if (AddName.Text != "" && AddNumber.Text != "" && AddType.Text != "" && AddLevel.Text != "" && AddHP.Text != "" &&
                AddAtt.Text != "" && AddDef.Text != "" && AddSpecAtt.Text != "" && AddSpecDef.Text != "" && AddSpeed.Text != "" && AddImg.Text != "")
            {

                Pokémon pok = new Pokémon()
                    {
                        Name = AddName.Text,
                        Number = Convert.ToInt32(AddNumber.Text),
                        Type = AddType.Text,
                        ImageUrl = AddImg.Text,
                        HP_Base = Convert.ToInt32(AddHP.Text),
                        Attack_Base = Convert.ToInt32(AddAtt.Text),
                        Defense_Base = Convert.ToInt32(AddDef.Text),
                        SpecialAttack_Base = Convert.ToInt32(AddSpecAtt.Text),
                        SpecialDefense_Base = Convert.ToInt32(AddSpecDef.Text),
                        Speed_Base = Convert.ToInt32(AddSpeed.Text),
                        Level = Convert.ToInt32(AddLevel.Text)
                    };
                MessageBox.Show("New Pokémon added!", "Added", MessageBoxButton.OK, MessageBoxImage.Information);
                this.Close();

            }
            else
            {
                MessageBox.Show("Not all of your fields are correctly filled in, please check this",
                                "One or more empty fields", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
