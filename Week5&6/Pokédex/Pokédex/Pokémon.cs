using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Pok�dex.Annotations;

namespace Pok�dex
{
    [Serializable()]
    public class Pok�mon : INotifyPropertyChanged
    {
        private int hp_Base;
        private int attack_Base;
        private int defense_Base;
        private int specialAttack_Base;
        private int specialDefense_Base;
        private int speed_Base;
        private int level;

        public string Name { get; set; }
        public int Number { get; set; }
        public string Type { get; set; }
        public string ImageUrl { get; set; }
        
        public int Level
        {
            get { return level; }
            set
            {
                level = value; 
                OnPropertyChanged();
                OnPropertyChanged("HP_Full");
                OnPropertyChanged("Attack_Full");
                OnPropertyChanged("Defense_Full");
                OnPropertyChanged("SpecialAttack_Full");
                OnPropertyChanged("SpecialDefense_Full");
                OnPropertyChanged("Speed_Full");
            }
        }
        public int HP_Base
        {
            get { return hp_Base; }
            set { hp_Base = value; OnPropertyChanged(); OnPropertyChanged("Average"); OnPropertyChanged("Total"); }
        }
        public int Attack_Base
        {
            get { return attack_Base; }
            set { attack_Base = value; OnPropertyChanged(); OnPropertyChanged("Attack_Full"); OnPropertyChanged("Average"); OnPropertyChanged("Total"); }
        }
	    public int Defense_Base
	    {
		    get { return defense_Base;}
            set { defense_Base = value; OnPropertyChanged(); OnPropertyChanged("Defense_Full");OnPropertyChanged("Average"); OnPropertyChanged("Total"); }
	    }
	    public int SpecialAttack_Base
	    {
		    get { return specialAttack_Base;}
            set { specialAttack_Base = value; OnPropertyChanged(); OnPropertyChanged("SpecialAttack_Full");OnPropertyChanged("Average"); OnPropertyChanged("Total"); }
	    }
	    public int SpecialDefense_Base
	    {
		    get { return specialDefense_Base;}
            set { specialDefense_Base = value; OnPropertyChanged(); OnPropertyChanged("SpecialDefense_Full");OnPropertyChanged("Average"); OnPropertyChanged("Total"); }
	    }
	    public int Speed_Base
	    {
		    get { return speed_Base;}
            set { speed_Base = value; OnPropertyChanged(); OnPropertyChanged("Speed_Full");OnPropertyChanged("Average"); OnPropertyChanged("Total"); }
	    }
        public int Average
        {
            get { return ((HP_Base+Attack_Base+Defense_Base +SpecialAttack_Base +SpecialDefense_Base +Speed_Base)/6); }
        }
        public int Total
        {
            get { return (HP_Base+Attack_Base+Defense_Base +SpecialAttack_Base +SpecialDefense_Base +Speed_Base); }
        }
        public int HP_Full
        {
            get { return ((((HP_Base + 50)*Level)/50) + 10); }
        }
        public int Attack_Full
        {
            get { return (((Attack_Base*Level)/50) + 5); }
        }
        public int Defense_Full
        {
            get { return (((Defense_Base * Level) / 50) + 5); }
        }
        public int SpecialAttack_Full
        {
            get { return (((SpecialAttack_Base * Level) / 50) + 5); }
        }
        public int SpecialDefense_Full
        {
            get { return (((specialDefense_Base * Level) / 50) + 5); }
        }
        public int Speed_Full
        {
            get { return (((Speed_Base * Level) / 50) + 5); }
        }
        public void VerhoogLevel()
        {
            Level++;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
