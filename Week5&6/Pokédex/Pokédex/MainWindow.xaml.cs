﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.Serialization;
using MahApps.Metro.Controls;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Pokédex
{

    public partial class MainWindow : MetroWindow
    {
        public ObservableCollection<Pokémon> Pokémons { get; set; }
        public MainWindow()
        {
            InitializeComponent();
            Pokémons = new ObservableCollection<Pokémon>();
                ////Pokémon worden bij laden venster reeds ingeladen, hieronder hardcoded van voorheen
                //{
                //    new Pokémon()
                //        {
                //            Name = "Bulbasaur",
                //            Number = 1,
                //            Type = "Grass",
                //            ImageUrl = "http://i903.photobucket.com/albums/ac234/GVSG/Pokemon%20PNGs/Bulbasaur.png",
                //            HP_Base = 45,
                //            Attack_Base = 49,
                //            Defense_Base = 49,
                //            SpecialAttack_Base = 65,
                //            SpecialDefense_Base = 65,
                //            Speed_Base = 45, 
                //            Level = 103
                //        },
                //    new Pokémon()
                //        {
                //            Name ="Charmander",
                //            Number =4,
                //            Type ="Fire",
                //            ImageUrl ="http://0-media-cdn.foolz.us/ffuuka/board/vp/image/1365/05/1365055899070.png",
                //            HP_Base =39,
                //            Attack_Base =52,
                //            Defense_Base =43,
                //            SpecialAttack_Base =60,
                //            SpecialDefense_Base =50,
                //            Speed_Base =65, 
                //            Level = 100
                //        },
                //    new Pokémon()
                //        {
                //            Name ="Squirtle",
                //            Number =7,
                //            Type ="Water",
                //            ImageUrl ="http://img3.wikia.nocookie.net/__cb20111211170902/villains/images/e/e3/Squirtle.png",
                //            HP_Base =44,
                //            Attack_Base =48,
                //            Defense_Base =65,
                //            SpecialAttack_Base =50,
                //            SpecialDefense_Base =64,
                //            Speed_Base =43, 
                //            Level = 90
                //        }
                //};

            var serializer = new JsonSerializer();
            using (var re = File.OpenText("json.txt"))
            using (var reader = new JsonTextReader(re))
            {
                var entries = serializer.Deserialize<Pokémon[]>(reader);
                foreach (var polkadanser in entries)
                {
                    Pokémons.Add(polkadanser);
                }
            }
            

            PokéFlip.DataContext = Pokémons;
            PokéStats.DataContext = Pokémons[0];
      
        }

        private void FlipView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var flipview = ((FlipView)sender);
            int sI = flipview.SelectedIndex;
            flipview.BannerText = Pokémons[sI].Name;
            PokéStats.DataContext = Pokémons[sI];
        }

        private void MetroWindow_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button) sender;
            if (btn.DataContext is Pokémon)
            {
                Pokémon pokemon = (Pokémon)btn.DataContext;
                pokemon.VerhoogLevel();
                //SerializePokémon(pokemon);
            }
        }

        private void SerializePokémon(Pokémon p)//poging om object te serializeren tot xml en zo als string op te slaan in settings... needs some work though ;-)
        {
            var serializer = new XmlSerializer(p.GetType());
            using (var writer = XmlWriter.Create("pokexml.xml"))
            {
                serializer.Serialize(writer, p);
            }
            
        }

        private void AddPoké_Click(object sender, RoutedEventArgs e)
        {
            AddPokémon addPokém = new AddPokémon();
            addPokém.ShowDialog();
            Pokémon pok = new Pokémon()
            {
                Name = addPokém.AddName.Text,
                Number = Convert.ToInt32(addPokém.AddNumber.Text),
                Type = addPokém.AddType.Text,
                ImageUrl = addPokém.AddImg.Text,
                HP_Base = Convert.ToInt32(addPokém.AddHP.Text),
                Attack_Base = Convert.ToInt32(addPokém.AddAtt.Text),
                Defense_Base = Convert.ToInt32(addPokém.AddDef.Text),
                SpecialAttack_Base = Convert.ToInt32(addPokém.AddSpecAtt.Text),
                SpecialDefense_Base = Convert.ToInt32(addPokém.AddSpecDef.Text),
                Speed_Base = Convert.ToInt32(addPokém.AddSpeed.Text),
                Level = Convert.ToInt32(addPokém.AddLevel.Text)
            };
            Pokémons.Add(pok);
            addPokém.Close();
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
             JsonSerializer serializer = new JsonSerializer();
             serializer.NullValueHandling = NullValueHandling.Ignore;
             
             using (StreamWriter sw = new StreamWriter(@"json.txt"))
             using (JsonWriter writer = new JsonTextWriter(sw))
            {
                serializer.Serialize(writer, Pokémons);
                // {"ExpiryDate":new Date(1230375600000),"Price":0}
            }
        }

        
    }
}
