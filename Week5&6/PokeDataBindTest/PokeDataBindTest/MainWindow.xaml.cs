﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PokeDataBindTest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public Pokémon bulbie;
        public Pokémon charmie;
        public Pokémon squirtie;
        public MainWindow()
        {
            InitializeComponent();
            
            bulbie = new Pokémon()
                {
                    Name = "Bulbasaur",
                    Number = 1,
                    Type = "Grass",
                    ImageUrl = "http://i903.photobucket.com/albums/ac234/GVSG/Pokemon%20PNGs/Bulbasaur.png",
                    HP_Base = 45,
                    Attack_Base = 49,
                    Defense_Base = 49,
                    SpecialAttack_Base = 65,
                    SpecialDefense_Base = 65,
                    Speed_Base = 45
                };
             charmie = new Pokémon()
                {
                    Name = "Charmander",
                    Number = 4,
                    Type = "Fire",
                    ImageUrl = "http://0-media-cdn.foolz.us/ffuuka/board/vp/image/1365/05/1365055899070.png",
                    HP_Base = 39,
                    Attack_Base = 52,
                    Defense_Base = 43,
                    SpecialAttack_Base = 60,
                    SpecialDefense_Base = 50,
                    Speed_Base = 65
                };
             squirtie = new Pokémon()
                {
                    Name = "Squirtle",
                    Number = 7,
                    Type = "Water",
                    ImageUrl = "http://img3.wikia.nocookie.net/__cb20111211170902/villains/images/e/e3/Squirtle.png",
                    HP_Base = 44,
                    Attack_Base = 48,
                    Defense_Base = 65,
                    SpecialAttack_Base = 50,
                    SpecialDefense_Base = 64,
                    Speed_Base = 43
                };
            Hoofd.DataContext = charmie;
        }

        public void Button_Click(object sender, RoutedEventArgs e)
        {
            Button cmd = (Button)sender;
            if (Hoofd.DataContext == charmie)
            {
                Hoofd.DataContext = bulbie;
            }
            else if (Hoofd.DataContext == bulbie)
            {
                Hoofd.DataContext = squirtie;
            }
            else
            {
                Hoofd.DataContext = charmie;
            }

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {}
    }
}
